package com.atul.graphlwebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphlWebapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphlWebapiApplication.class, args);
	}

}

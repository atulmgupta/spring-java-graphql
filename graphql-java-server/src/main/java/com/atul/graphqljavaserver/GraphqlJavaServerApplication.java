package com.atul.graphqljavaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlJavaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlJavaServerApplication.class, args);
	}

}

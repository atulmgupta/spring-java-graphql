package com.atul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphlEntityApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphlEntityApplication.class, args);
	}

}

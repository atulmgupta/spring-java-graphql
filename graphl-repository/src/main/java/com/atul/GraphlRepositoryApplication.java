package com.atul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphlRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphlRepositoryApplication.class, args);
	}

}
